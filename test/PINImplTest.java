
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import java_Training.home_work_2.PINImpl;
/**
 * right file
 * @author Vratas
 *
 */
public class PINImplTest {

	PINImpl PIN;

	@Before
	public void setUp() throws Exception {
		PIN = new PINImpl();
	}

	@Test
	public void test_checkPIN_S_rightPIN_longAndShorFormat() {
		assertTrue(PIN.checkPIN_S("550527-8787"));
		assertTrue(PIN.checkPIN_S("20101010-0004"));
	}
	
	@Test
	public void test_checkPIN_S_badPIN_longAndShorFormat() {
		assertFalse(PIN.checkPIN_S("550527-8788"));
		assertFalse(PIN.checkPIN_S("20101010-0005"));
		assertFalse(PIN.checkPIN_S("999999-9999"));
	}
	
	@Test
	public void test_checkPIN_S_rightPIN_correctDate() {
		assertTrue(PIN.checkPIN_S("19400229-0007"));
		assertTrue(PIN.checkPIN_S("19750331-0018"));
	}
	
	@Test
	public void test_checkPIN_S_rightPIN_incorrectDate() {
		assertFalse(PIN.checkPIN_S("19410229-0006"));
		assertFalse(PIN.checkPIN_S("19410431-0007"));
	}
	
	@Test
	public void test_checkPIN_S_rightPIN_wrongFormat() {
		assertFalse(PIN.checkPIN_S("19410229-AA07"));
		assertFalse(PIN.checkPIN_S("1941AA29-AA07"));
		assertFalse(PIN.checkPIN_S("19410229-7"));
		assertFalse(PIN.checkPIN_S("19410229-123456007"));
	}
	
	@Test
	public void test_checkPIN_S_rightPIN_withUnnecessarySigns() {
		assertTrue(PIN.checkPIN_S("1940-02 29 0007"));
		assertTrue(PIN.checkPIN_S("(1940-02-29@0007.)"));
		assertTrue(PIN.checkPIN_S("1940*02_29/-0007.."));
		assertTrue(PIN.checkPIN_S("\"1940*02_29/-0007..\""));
	}
	
	@Test
	public void test_checkPIN_S_rightPIN_Stranger() {
		assertTrue(PIN.checkPIN_S("750565-1120"));
	}
	
	@Test
	public void test_checkPIN_S_null() {
		assertFalse(PIN.checkPIN_S("null"));
	}
	
	
	@Test
	public void test_checkPIN_CZ_rightPIN_longAndShorFormat() {
		assertTrue(PIN.checkPIN_CZ("750320-5765"));
		assertTrue(PIN.checkPIN_CZ("485715/776"));
	}
	
	@Test
	public void test_checkPin_CZ_badPIN_longAndShorFormat() {
		assertFalse(PIN.checkPIN_CZ("750320-5766"));
		assertFalse(PIN.checkPIN_CZ("999999-9999"));
	}
	
	@Test
	public void test_checkPin_CZ_rightPIN_correctDate() {
		assertTrue(PIN.checkPIN_CZ("485229/776"));
		assertTrue(PIN.checkPIN_CZ("815331/7766"));
	}
	
	@Test
	public void test_checkPin_CZ_rightPIN_incorrectDate() {
		assertFalse(PIN.checkPIN_CZ("475229/776"));
		assertFalse(PIN.checkPIN_CZ("815431/7766"));
		assertFalse(PIN.checkPIN_CZ("2510101110"));		
	}
	
	@Test
	public void test_checkPin_CZ_rightPIN_wrongFormat() {
		assertFalse(PIN.checkPIN_CZ("810331/776A"));
		assertFalse(PIN.checkPIN_CZ("AA0331/7761"));
		assertFalse(PIN.checkPIN_CZ("810331/77"));
		assertFalse(PIN.checkPIN_CZ("810331/776112"));
	}
	
	@Test
	public void test_checkPIN_CZ_rightPIN_withUnnecessarySigns() {
		assertTrue(PIN.checkPIN_CZ("810331 7761"));
		assertTrue(PIN.checkPIN_CZ("(81--03-31@7761.)"));
		assertTrue(PIN.checkPIN_CZ("81*03_31/-7761.."));
		assertTrue(PIN.checkPIN_CZ("\"81*03_31/-7761..\""));
	}
	
	@Test
	public void test_checkPin_CZ_rightPIN_foreign() {
		assertTrue(PIN.checkPIN_CZ("550278/4100))"));
	}
	
	@Test
	public void test_checkPin_CZ_badPIN_foreign() {
		assertFalse(PIN.checkPIN_CZ("500298/4096"));
	}
	
	@Test
	public void test_checkPin_CZ_null() {
		assertFalse(PIN.checkPIN_CZ("null"));
	}	
}
