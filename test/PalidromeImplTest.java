

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import java_Training.home_work_1.PalindromeImpl;
/**
 * Testing 3 methods in class PalindromeImpl 
 * @author PC-Obyvak
 */

public class PalidromeImplTest {
	
	private PalindromeImpl pal;
	
	@Before
	public void setUp() throws Exception {
		pal = new PalindromeImpl();	
	}

	@Test
	public void test_isPalindrome_UsualWords() {
		assertTrue(pal.isPalindrome("kajak"));
		assertTrue(pal.isPalindrome("radar"));
	}
		
	@Test
	public void test_isPalindrome_WordsWithSpaces() {
		assertTrue(pal.isPalindrome("kobyla ma   maly bok"));
		assertTrue(pal.isPalindrome("ra d    a r"));
		assertTrue(pal.isPalindrome("a a b b a a "));
	}
	
	@Test
	public void test_isPalindrome_WordsWithSmallandBigLetter() {
		assertTrue(pal.isPalindrome("Kobyla ma MALY bok"));
		assertTrue(pal.isPalindrome("aAA"));
	}
	
	@Test
	public void test_isPalindrome_WordsWithDiacritics() {
		assertTrue(pal.isPalindrome("kobyla m� mal� bok"));
		assertTrue(pal.isPalindrome("���rcs"));
		assertTrue(pal.isPalindrome("�A�"));
	}
		
	@Test
	public void test_isPalindrome_WordsWithNumber() {
		assertFalse(pal.isPalindrome("123321"));
		assertFalse(pal.isPalindrome("aa1aa"));
	}
	
	@Test
	public void test_isPalindrome_WordsWithSpecialCharacters() {
		assertTrue(pal.isPalindrome(" Kobyla \"m� \", *(mal�)* bok.!?;"));
	}
		
	@Test
	public void test_isPalindrome_badWords() {
		assertFalse(pal.isPalindrome("bulshit"));
		assertFalse(pal.isPalindrome("a a a b"));
	}
	
	@Test
	public void test_isPalindrome_WordsLessThan3characters() {
		assertFalse(pal.isPalindrome("AA"));
		assertFalse(pal.isPalindrome("AB"));
		assertFalse(pal.isPalindrome("a"));
		assertFalse(pal.isPalindrome(""));
	}
		
	@Test
	public void test_isPalindrome_null() {
		pal = new PalindromeImpl();
		assertFalse(pal.isPalindrome(null));
	}
	
	@Test
	public void test_CountPalindrome_Arraylist() {
		/**
		 * 7 items from ArrayList are right palindromes
		 */
		ArrayList<String> alistTest = new ArrayList<String>();
		alistTest.add("Kajak");alistTest.add("Kobyla m� mal� bok");alistTest.add(" Radar ");alistTest.add("      Kobyla m� mal� bok  ");
		alistTest.add("blbost");alistTest.add("");alistTest.add("123321");alistTest.add("AA");alistTest.add("@@@");alistTest.add("-11+22");
		alistTest.add("null");alistTest.add("���rcs");alistTest.add(" (Kajak!?)");alistTest.add(" __(Radar.)");	
		assertEquals(7, pal.countPalindromes(alistTest));
	}
	
	@Test
	public void test_ArrayListPalindrome_Arraylist() {
		ArrayList<String> source = new ArrayList<String>();
		source.add("Kajak");source.add("Kobyla m� mal� bok");source.add(" Radar ");source.add("      Kobyla m� mal� bok  ");
		source.add("blbost");source.add("");source.add("123321");source.add("AA");source.add("@@@");source.add("-11+22");
		source.add("null");source.add("���rcs");source.add(" (Kajak!?)");source.add(" __(Radar.)");		
		
		ArrayList<String> forControl = new ArrayList<String>();
		forControl.add("Kajak");forControl.add("Kobyla m� mal� bok");forControl.add(" Radar ");forControl.add("      Kobyla m� mal� bok  ");
		forControl.add("���rcs");forControl.add(" (Kajak!?)");forControl.add(" __(Radar.)");		
		
		ArrayList<String> expected = new ArrayList<String>();
			for (String s1 : pal.getPalindromes(source)) {
				expected.add(s1); 
			}
		/**
		 * Compare ArrayList of result with ArrayList with right palindromes (expected)
		 */
		assertTrue(expected.containsAll(forControl));
		assertEquals(expected, forControl);
	}
	
}
