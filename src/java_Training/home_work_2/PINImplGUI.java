package java_Training.home_work_2;
/**
 * right file
 * GUI
 */
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ImageIcon;
import java.awt.SystemColor;

/**
 * right file
 * @author VratasL
 * GUI
 */
public class PINImplGUI {

	private JFrame frame;
	private JTextField textField_Denmark;
	private JTextField textField_Czech;
	private JTextField textField_Sweden;
	private JTextField textField_Norway;
	private JTextField textField_Finland;
	private JLabel Czech;
	private JLabel Sweden;
	private JLabel Finland;
	private JLabel Norway;
	private JLabel Denmark;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PINImplGUI window = new PINImplGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PINImplGUI() {
		initialize();
	}
	PINImpl pin = new PINImpl();
	private JLabel label_Czech;
	private JLabel label_Sweden;
	private JLabel label_Finland;
	private JLabel label_Denmark;
	private JLabel label_Norway;
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setSize(new Dimension(400, 400));
		frame.getContentPane().setFont(new Font("Tahoma", Font.PLAIN, 12));
		frame.setTitle("Check PIN");
		frame.setBounds(100, 100, 412, 445);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Czech = new JLabel("Czech");
		Czech.setFont(new Font("Tahoma", Font.PLAIN, 20));
		Czech.setAlignmentY(1.0f);
		Czech.setAlignmentX(1.0f);
		
		textField_Czech = new JTextField();
		textField_Czech.setBorder(new LineBorder(Color.BLACK));
		textField_Czech.setFont(new Font("Tahoma", Font.PLAIN, 20));
		textField_Czech.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				checkPINczech();
			}
			@Override
			public void keyReleased(KeyEvent e) {
				checkPINczech();
			}
			@Override
			public void keyTyped(KeyEvent e) {
				checkPINczech();
			}
			private void checkPINczech(){
				if (pin.checkPIN_CZ(textField_Czech.getText())) {
					
					label_Czech.setIcon(UIManager.getIcon("OptionPane.informationIcon"));
				}
				else {
					
					label_Czech.setIcon(UIManager.getIcon("OptionPane.errorIcon"));
				}
			}
			
		});
		textField_Czech.setColumns(12);
		
		Sweden = new JLabel("Sweden");
		Sweden.setFont(new Font("Tahoma", Font.PLAIN, 20));
		Sweden.setAlignmentY(1.0f);
		Sweden.setAlignmentX(1.0f);
		
		textField_Sweden = new JTextField();
		textField_Sweden.setBorder(new LineBorder(Color.BLACK));
		textField_Sweden.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				checkPINswedish();
			}
			@Override
			public void keyReleased(KeyEvent e) {
				checkPINswedish();
			}
			@Override
			public void keyTyped(KeyEvent e) {
				checkPINswedish();
			}
			private void checkPINswedish(){
				if (pin.checkPIN_S(textField_Sweden.getText())) {
					label_Sweden.setIcon(UIManager.getIcon("OptionPane.informationIcon"));
				}
				else {
					label_Sweden.setIcon(UIManager.getIcon("OptionPane.errorIcon"));
				}
			}
		});
		textField_Sweden.setFont(new Font("Tahoma", Font.PLAIN, 20));
		textField_Sweden.setColumns(12);
		
		Norway = new JLabel("Norway");
		Norway.setFont(new Font("Tahoma", Font.PLAIN, 20));
		Norway.setAlignmentY(1.0f);
		Norway.setAlignmentX(1.0f);
		
		textField_Norway = new JTextField();
		textField_Norway.setBorder(new LineBorder(Color.BLACK));
		textField_Norway.setFont(new Font("Tahoma", Font.PLAIN, 20));
		textField_Norway.setColumns(12);
		
		Finland = new JLabel("Finland");
		Finland.setFont(new Font("Tahoma", Font.PLAIN, 20));
		Finland.setAlignmentY(1.0f);
		Finland.setAlignmentX(1.0f);
		
		textField_Finland = new JTextField();
		textField_Finland.setBorder(new LineBorder(Color.BLACK));
		textField_Finland.setFont(new Font("Tahoma", Font.PLAIN, 20));
		textField_Finland.setColumns(12);
		
		Denmark = new JLabel("Denmark");
		Denmark.setFont(new Font("Tahoma", Font.PLAIN, 20));
		Denmark.setAlignmentY(1.0f);
		Denmark.setAlignmentX(1.0f);
		
		textField_Denmark = new JTextField();
		textField_Denmark.setBorder(new LineBorder(Color.BLACK));
		textField_Denmark.setFont(new Font("Tahoma", Font.PLAIN, 20));
		textField_Denmark.setColumns(12);
		
		label_Czech = new JLabel("");
		label_Czech.setAlignmentY(Component.BOTTOM_ALIGNMENT);
		label_Czech.setFont(new Font("Tahoma", Font.PLAIN, 12));
		label_Czech.setBorder(new LineBorder(new Color(0, 0, 0)));
		
		label_Sweden = new JLabel("");
		label_Sweden.setFont(new Font("Tahoma", Font.PLAIN, 12));
		label_Sweden.setBorder(new LineBorder(new Color(0, 0, 0)));
		label_Sweden.setAlignmentY(1.0f);
		
		label_Norway = new JLabel((String) null);
		label_Norway.setForeground(Color.BLACK);
		label_Norway.setBackground(SystemColor.menu);
		label_Norway.setFont(new Font("Tahoma", Font.PLAIN, 12));
		label_Norway.setBorder(new LineBorder(new Color(0, 0, 0)));
		label_Norway.setAlignmentY(1.0f);
		
		label_Finland = new JLabel("     ");
		label_Finland.setFont(new Font("Tahoma", Font.PLAIN, 12));
		label_Finland.setBorder(new LineBorder(new Color(0, 0, 0)));
		label_Finland.setAlignmentY(1.0f);
		
		label_Denmark = new JLabel("     ");
		label_Denmark.setFont(new Font("Tahoma", Font.PLAIN, 12));
		label_Denmark.setBorder(new LineBorder(new Color(0, 0, 0)));
		label_Denmark.setAlignmentY(1.0f);
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
							.addGroup(groupLayout.createSequentialGroup()
								.addComponent(Czech, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(textField_Czech, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)
								.addGap(6))
							.addGroup(groupLayout.createSequentialGroup()
								.addComponent(Sweden, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
								.addGap(18)
								.addComponent(textField_Sweden, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGap(4)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(Denmark, GroupLayout.PREFERRED_SIZE, 83, GroupLayout.PREFERRED_SIZE)
								.addComponent(Finland, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
								.addComponent(Norway, GroupLayout.PREFERRED_SIZE, 83, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(textField_Denmark, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(textField_Finland, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(textField_Norway, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
					.addGap(27)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
							.addComponent(label_Sweden, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
							.addComponent(label_Czech, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE))
						.addComponent(label_Norway, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_Finland, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_Denmark, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE))
					.addGap(17))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(5)
									.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
										.addComponent(Czech)
										.addComponent(label_Czech, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
										.addComponent(textField_Czech, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(label_Sweden, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(47)
									.addComponent(Sweden)))
							.addGap(85)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(textField_Finland, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(Finland)
								.addComponent(label_Finland, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
									.addComponent(Denmark)
									.addComponent(label_Denmark, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
								.addComponent(textField_Denmark, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(51)
							.addComponent(textField_Sweden, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(textField_Norway, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(label_Norway, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
								.addComponent(Norway))))
					.addContainerGap(186, Short.MAX_VALUE))
		);
		frame.getContentPane().setLayout(groupLayout);
	}
}
