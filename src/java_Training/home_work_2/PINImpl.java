package java_Training.home_work_2;
/**
 * right file
 */
import java.util.Calendar;
import java.util.GregorianCalendar;

public class PINImpl implements PIN {
	
	public String testPIN;			//public variable only for class TemporarySOPforControl
	@Override
	public boolean checkPIN_S(String sourcePIN) {
		testPIN=sourcePIN;
		int index100=0;
		
		if (sourcePIN==null) {
			return false;
		}
		if (sourcePIN.contains("+")) {
			index100=100;
		}
		sourcePIN=withoutSigns(sourcePIN);
		
		if (!controlIsOnlyNumber(sourcePIN)) {
			return false;
		}
		if (!(sourcePIN.length()==10 || sourcePIN.length()==12)) {
			return false;
		}
		if (!swedishValidateDate(sourcePIN,index100)){
			return false;
		}
		if (sourcePIN.length()==12){
			sourcePIN=sourcePIN.substring(2,12);
		}	
		if (!swedishValidateCheckSum(sourcePIN)) {
			return false;
		}
	return true;	
	}	
	
	@Override
	public boolean checkPIN_CZ(String sourcePIN) {		
		testPIN=sourcePIN;
		if (sourcePIN==null) {
			return false;
		}
		sourcePIN=withoutSigns(sourcePIN);
		if (!controlIsOnlyNumber(sourcePIN)) {
			return false;
		}
		if (sourcePIN.length()<9 || sourcePIN.length()>10) {
			return false;
		}
		if (!czechValidateDate(sourcePIN)){
			return false;
		}
		int yy=Integer.parseInt(sourcePIN.substring(0,2));
		if (sourcePIN.length()==10) { 
			if (!czechvalidateDivide11(sourcePIN,yy)) {
				return false;	
			}
		}		
		return true;
	}
	
	/**
	 * use calendar for Swedish check date
	 * @param inputPIN, index100
	 * @return true if date from sourcePIN is correctly
	 */
	private boolean swedishValidateDate(String inputPIN, int index100) {
		Calendar pinCalendar = new GregorianCalendar();
		Calendar todayCalendar = Calendar.getInstance();
		int index=0;
		index=inputPIN.length();
		int yy=Integer.parseInt(inputPIN.substring(0,index-8));
		int mm=Integer.parseInt(inputPIN.substring(index-8,index-6));
		int dd=Integer.parseInt(inputPIN.substring(index-6,index-4));
		//only for stranger in Sweden
		if (dd>60) {
			dd-=60;
		}
		//use calendar for check date from PIN
		try{
		    pinCalendar.setLenient(false);
		    if (inputPIN.length()==10 && yy>(todayCalendar.get(Calendar.YEAR)-2000)){
		    	yy+=1900;
		    }
			else if (inputPIN.length()==10 && yy<(todayCalendar.get(Calendar.YEAR)-1999)) {
				yy+=2000;
			}	
		    pinCalendar.set(yy,mm-1,dd);
		    	if (index100==100) {
		    		pinCalendar.add(Calendar.YEAR,-100);
		    	}
		    //this call exception
		    pinCalendar.getTime();	
			}
		catch (Exception e){		
			return false;
		}
		if (pinCalendar.after(todayCalendar)) {
			return false;
		}
		return true;
	}
	
	
	/**
	 * use calendar for Czech check date
	 * @param inputPIN
	 * @return true if date from sourcePIN is correctly
	 */
	private boolean czechValidateDate(String inputPIN) {
		Calendar pinCalendar = new GregorianCalendar();
		Calendar todayCalendar = Calendar.getInstance();
		int yy=Integer.parseInt(inputPIN.substring(0,2));
		int mm=Integer.parseInt(inputPIN.substring(2,4));
		int dd=Integer.parseInt(inputPIN.substring(4,6));		
		int signOfForeigner=Integer.parseInt(inputPIN.substring(6,8));
		
		//check day dd, can be: 1-31, 51-82(for strangers)		
		if (dd>31){
			dd= czechValidateDay(dd, signOfForeigner);
		}
		//check month mm, can be: 1-12, 21-32, 51-62, 71-82
		if (mm>12){
			mm= czechValidateMonth(mm);	
		}
		//use calendar for check date from PIN
		try{
			pinCalendar.setLenient(false);
			if ((inputPIN.length()==9 && yy<55)|| ((inputPIN.length()==10) && yy>54)){
		    	yy+=1900;
		    }
			else if ((inputPIN.length()==10) && yy<54){
				yy+=2000;
			}
			else {
				return false;
			}
		   	pinCalendar.set(yy,mm-1,dd);		  
		   	//this call exception
		   	pinCalendar.getTime();
			}
		catch (Exception e){
			return false;
		}
		//to 1954 had Czech PIN numbers lenght 9, after 1954 is lenght 10 positions
		if (inputPIN.length()==9 && (pinCalendar.get(Calendar.YEAR)>1953)){
			return false;
		}
		if (pinCalendar.after(todayCalendar)) {
			return false;
		}
		return true;
	}


	/**
	 * @param source
	 * @return string without unnecessary characters
	 */
	String withoutSigns(String source){
		source=source.replaceAll("[-+/ _().?!;*@\\\"]","");
		return source;
	}
	/**
	 * @param source
	 * @return true if source is digits
	 */
	boolean controlIsOnlyNumber (String source){
		for (char sTemp : source.toCharArray()) {
			if (!Character.isDigit(sTemp)) {
				return false;
			} 
		}
		return true;
	}
	
	/**
	 * Validate Swedish PIN 
	 * string -> string array -> integer array and calculation for validation
	 * @param string swedishInput
	 * @return true if special sum of input is divided by 10
	 */
	private boolean swedishValidateCheckSum(String swedishInput) {
		String[] swedishStrings = swedishInput.split("");
		int[] swedishInts = new int[swedishInput.length()];
		int swedishTemp =0, swedishSum=0;
		for (int i = 0; i < swedishInput.length(); i++) {
			swedishInts[i] = Integer.parseInt(swedishStrings[i]);
			if (i%2==0) {
				swedishTemp=swedishInts[i]*2;
					if (swedishTemp>9) {
						swedishTemp-=9;
					}
					
				}
			else {
				swedishTemp=swedishInts[i];
			}
			swedishSum+=swedishTemp;
		}
		if (swedishSum%10==0) { 
			return true;
		} 
		return false;
	}

	/**
	 * validate Czech PIN by calculation
	 * @param pin
	 * @param year
	 * @return true if PIN is divided by 11 
	 */
	boolean czechvalidateDivide11(String czechInput, int year) {
		long czechPIN =Long.parseLong(czechInput);
		if (czechPIN%10==0)  {
			if ((czechPIN/10)%11==0 && year<86) {
				return true;
			}
			else {
				return false;
			}
		}
		if (czechPIN%11==0) {	
			return true;
		}
		return false;
	}
		
	/**
	 * @param day, signForeign
	 * @return day if condition OK right number, or -1 
	 */
	Integer czechValidateDay(int day, int signForeign){
		if ((day>50 && day<82) && (signForeign>39 && signForeign<44)) {
			day-=50;
		}
		if (day>0 && day<32) {
			return day;
		}
		return day=-1;
	}
	
	/**
	 * @param month
	 * @return month if condition OK right number, or -1 
	 */		
	Integer czechValidateMonth(int month){
		if (month>50) {
			month-=50;
		}
		if (month>20) {
			month-=20;
		}		
		if (month>0 && month<13) {
			return month;
		}
		return month=-1;
	}
}			
