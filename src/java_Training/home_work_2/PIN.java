package java_Training.home_work_2;
/**
 * right file
 * For checking PIN (Czech and Swedish)
 * @author Vratas
 */
public interface PIN {
	/**
	 * @param source string number for check 
	 * @return true if input string (number) is right PIN(S)
	 */
	public boolean checkPIN_S(String source);
	/**
	 * @param source string number for check 
	 * @return true if input string (number) is right PIN(CZ)
	 */
	public boolean checkPIN_CZ(String source);
}
