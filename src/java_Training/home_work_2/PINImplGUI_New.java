package java_Training.home_work_2;
/**
 * right file
 * Easy gui
 */

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class PINImplGUI_New {
	
public static void main(String[] args) {
		 
		JFrame frame = new JFrame();		
		frame.setTitle("PIN tester");
		frame.setResizable(false);
		
		frame.setLocation(100, 100);
	    frame.setSize(400, 300);
		//frame.setBounds(100, 100, 412, 445);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        
        PINImpl pin = new PINImpl();
        
        JLabel Czech = new JLabel("Czech");
        JTextField inputFieldCzech = new JTextField("", 15);
        JLabel imagelabelCzech = new JLabel();
        imagelabelCzech.setPreferredSize(new Dimension(36, 36));
        imagelabelCzech.setIcon(UIManager.getIcon("OptionPane.errorIcon"));
        
        inputFieldCzech.getDocument().addDocumentListener(new DocumentListener() {
	        @Override
			public void removeUpdate(DocumentEvent e) {
				czechPINcontrol();
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				czechPINcontrol();
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				czechPINcontrol();
			}
			
			private void czechPINcontrol()
			{
				if(pin.checkPIN_CZ(inputFieldCzech.getText())) {
					imagelabelCzech.setIcon(UIManager.getIcon("OptionPane.informationIcon"));
				} else {
					imagelabelCzech.setIcon(UIManager.getIcon("OptionPane.errorIcon"));
				}				
			}
		});
        
        JLabel Sweden = new JLabel("Sweden");
        //Sweden.setAlignmentY(1.0f);
		//Sweden.setAlignmentX(1.0f);
		Sweden.setAlignmentX(Component.RIGHT_ALIGNMENT);
		Sweden.setAlignmentY(Component.BOTTOM_ALIGNMENT);
		//frame.getContentPane().add(Sweden);
        JTextField inputFieldSweden = new JTextField("", 15);
        JLabel imagelabelSweden = new JLabel();
        imagelabelSweden.setPreferredSize(new Dimension(36, 36));
        imagelabelSweden.setIcon(UIManager.getIcon("OptionPane.errorIcon"));
        
        inputFieldSweden.getDocument().addDocumentListener(new DocumentListener() {

        @Override
		public void removeUpdate(DocumentEvent e) {
			swedishPINcontrol();
		}
		
		@Override
		public void insertUpdate(DocumentEvent e) {
			swedishPINcontrol();
		}
		
		@Override
		public void changedUpdate(DocumentEvent e) {
			swedishPINcontrol();
		}
		
		private void swedishPINcontrol()
		{
			if(pin.checkPIN_S(inputFieldSweden.getText())) {
				imagelabelSweden.setIcon(UIManager.getIcon("OptionPane.informationIcon"));
			} 	
				else {
				imagelabelSweden.setIcon(UIManager.getIcon("OptionPane.errorIcon"));
				}				
			}         
      });
        
        JPanel panel = new JPanel();
        panel.add(Czech);
        panel.add(inputFieldCzech);
        panel.add(imagelabelCzech);
        panel.add(Sweden);
        panel.add(inputFieldSweden);
        panel.add(imagelabelSweden);
        frame.setContentPane(panel);        
        frame.pack();
        frame.setVisible(true);
	}

}
