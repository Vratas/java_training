package java_Training.home_work_1;

import java.util.Collection;

public interface Palindrome {
	/**
	 * @param s string for check 
	 * @return true if variable s is palindrome
	 */
	public boolean isPalindrome(String s);
	/**
	 * @param collection<String> for check (calling method Palindrome)
	 * @return count of palindromes	 
	 */
	public int countPalindromes(Collection<String> words);	
	/**
	 * @param collection<String> for check (calling method Palindrome)
	 * @return new collection<String> with right palindromes 
	 */
	public Collection<String> getPalindromes(Collection<String> words);
}
