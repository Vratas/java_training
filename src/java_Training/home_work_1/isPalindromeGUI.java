package java_Training.home_work_1;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

public class isPalindromeGUI {

	private JFrame frmPalindromeTest;
	private JLabel lblZadejText;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					isPalindromeGUI window = new isPalindromeGUI();
					window.frmPalindromeTest.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public isPalindromeGUI() {
		// TODO Auto-generated constructor stub
		initialize();	
}
	PalindromeImpl pal = new PalindromeImpl();
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmPalindromeTest = new JFrame();
		frmPalindromeTest.setTitle("Palindrome test");
		frmPalindromeTest.setBounds(100, 100, 650, 100);
		frmPalindromeTest.setResizable(false);
		frmPalindromeTest.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		lblZadejText = new JLabel("Zadej text pro zji�t�n� :");
		lblZadejText.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblZadejText.setHorizontalAlignment(SwingConstants.CENTER);
		lblZadejText.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		frmPalindromeTest.getContentPane().add(lblZadejText, BorderLayout.NORTH);
		
		JLabel lblResult = new JLabel("");
		lblResult.setPreferredSize(new Dimension(50, 14));
		lblResult.setIcon(UIManager.getIcon("OptionPane.errorIcon"));
		
		textField = new JTextField();
		textField.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				textField.setText("");
				lblResult.setIcon(UIManager.getIcon("OptionPane.errorIcon"));
			}
		});
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				check();	
			}
			@Override
			public void keyReleased(KeyEvent e) {
				check();
			}
			@Override
			public void keyTyped(KeyEvent e) {
				check();
			}
			private void check(){
				if (pal.isPalindrome(textField.getText())) {
					//lblResult.setText("OK");
					lblResult.setIcon(UIManager.getIcon("OptionPane.informationIcon"));
				}
				else {
					lblResult.setIcon(UIManager.getIcon("OptionPane.errorIcon"));
				}
			}
		});
		textField.setFont(new Font("Tahoma", Font.PLAIN, 18));
		frmPalindromeTest.getContentPane().add(textField, BorderLayout.CENTER);
		textField.setColumns(2);
		frmPalindromeTest.getContentPane().add(lblResult, BorderLayout.EAST);
	}

}
