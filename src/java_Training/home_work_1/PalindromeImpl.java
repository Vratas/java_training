package java_Training.home_work_1;
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.ArrayList;
import java.util.Collection;

public class PalindromeImpl implements Palindrome {
		
	public String textTemp;			//public variable only for class TestPalindereImpl
	
	public boolean isPalindrome(String source) {
		textTemp=source;	// @String textTemp (temporary public variable) only for class TestPalindereImpl 
		if (source=="" || source==null) {
			return false;
		}
		for (char sTemp : source.toCharArray()) {
			if (Character.isDigit(sTemp)) {
				return false;
			} 
		}
		String text=source.toLowerCase().replaceAll("[ .!?;,-_*@()\"]","");
		
		if (text.length()<3) {
			return false;
		}		
		text=Normalizer.normalize(text,Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");    	
		String textBackward=new StringBuffer(text).reverse().toString();    	
		if (text.equals(textBackward)) {
			return true;
		}
			else {
				return false;
			}
    	}
	
	public int countPalindromes(Collection<String> alistTemp) {
		int count=0;
		for (String s : alistTemp) 	{
			if (isPalindrome(s)){ 
				count++;
			}
		}
		return count;
	}

	public Collection<String> getPalindromes(Collection<String> words) {
		ArrayList<String> result = new ArrayList<String>();		
		for (String s : words) {
			if (isPalindrome(s)) {
				result.add(s);
			}
		}
		return result;
	}

	
}
