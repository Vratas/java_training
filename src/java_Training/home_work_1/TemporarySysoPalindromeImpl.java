package java_Training.home_work_1;
import java.util.ArrayList;
/**
 * Class is only for writing to console and testing class PalindromeImpl
 * If this class be removed, remove form class PalindromeImpl variable @String textTemp (temporary public variable)
 * @author PC-Obyvak
 *
 */
public class TemporarySysoPalindromeImpl {

	public static void main(String[] args) {
		
		boolean OK=false;
		
		ArrayList<String> alist1 = new ArrayList<String>();
		alist1.add("Kajak");alist1.add("Kobyla m� mal� bok");alist1.add(" Radar ");
		alist1.add("      Kobyla m� mal� bok  ");alist1.add("blbost");alist1.add("");
		alist1.add("123321");alist1.add("AA");alist1.add("@@@");alist1.add("-11+22");
		alist1.add("null");alist1.add(" (Kajak!?)");alist1.add(" __(Radar.)");
		   
		PalindromeImpl p1 = new PalindromeImpl();
		
		System.out.println("---------- 1.method ------------");
		OK=p1.isPalindrome("Kajak");
		if (OK) System.out.println(p1.textTemp + "\t\t\t\t - palindrome OK");
		else System.out.println(p1.textTemp +"\t\t\t\t - is not palindrome");
		OK=p1.isPalindrome("Kobyla m� mal� bok");
		if (OK) System.out.println(p1.textTemp + "\t\t\t\t - palindrome OK");
		else System.out.println(p1.textTemp +"\t\t\t\t - is not palindrome");
		OK=p1.isPalindrome(" Ra1ar ");
		if (OK) System.out.println(p1.textTemp + "\t\t\t\t - palindrome OK");
		else System.out.println(p1.textTemp +"\t\t\t\t - is not palindrome");
		OK=p1.isPalindrome("   ()Kobyla   m� mal� bok . ");
		if (OK) System.out.println(p1.textTemp + "\t\t\t\t - palindrome OK");
		else System.out.println(p1.textTemp +"\t\t\t\t - is not palindrome");
		OK=p1.isPalindrome("���rcs");
		if (OK) System.out.println(p1.textTemp + "\t\t\t\t - palindrome OK");
		else System.out.println(p1.textTemp +"\t\t\t\t - is not palindrome");
		OK=p1.isPalindrome("blbost");
		if (OK) System.out.println(p1.textTemp + "\t\t\t\t - palindrome OK");
		else System.out.println(p1.textTemp +"\t\t\t\t - is not palindrome");
		OK=p1.isPalindrome("");
		if (OK) System.out.println(p1.textTemp + "\t\t\t\t - palindrome OK");
		else System.out.println(p1.textTemp +"\t\t\t\t - is not palindrome");
		OK=p1.isPalindrome("123321");
		if (OK) System.out.println(p1.textTemp + "\t\t\t\t - palindrome OK");
		else System.out.println(p1.textTemp +"\t\t\t\t - is not palindrome");
		OK=p1.isPalindrome("AA");
		if (OK) System.out.println(p1.textTemp + "\t\t\t\t - palindrome OK");
		else System.out.println(p1.textTemp +"\t\t\t\t - is not palindrome");
		OK=p1.isPalindrome("@@@");
		if (OK) System.out.println(p1.textTemp + "\t\t\t\t - palindrome OK");
		else System.out.println(p1.textTemp +"\t\t\t\t - is not palindrome");
		OK=p1.isPalindrome("-11+22");
		if (OK) System.out.println(p1.textTemp + "\t\t\t\t - palindrome OK");
		else System.out.println(p1.textTemp +"\t\t\t\t - is not palindrome");
		OK=p1.isPalindrome(null);
		if (OK) System.out.println(p1.textTemp + "\t\t\t\t - palindrome OK");
		else System.out.println(p1.textTemp +"\t\t\t\t - is not palindrome");
		OK=p1.isPalindrome(" a b A");
		if (OK) System.out.println(p1.textTemp + "\t\t\t\t - palindrome OK");
		else System.out.println(p1.textTemp +"\t\t\t\t - is not palindrome");
		OK=p1.isPalindrome(" (kajak.)");
		if (OK) System.out.println(p1.textTemp + "\t\t\t\t - palindrome OK");
		else System.out.println(p1.textTemp +"\t\t\t\t - is not palindrome");
		
		System.out.println();
		System.out.println("---------- 2.method ------------");
		System.out.println("Count of palindromes from ArrayList is : "+p1.countPalindromes(alist1));
		System.out.println();
		System.out.println("---------- 3.method ------------");
		for (String s1 : p1.getPalindromes(alist1))
			System.out.println(s1);
		System.out.println();
		System.out.println("-----writing original ArrayList for checking (if is not change)-----");
		for (String s2 : alist1)
			System.out.println(s2);
		
	}

}
